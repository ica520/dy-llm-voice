<!--
 * @Author: ica caijianling@outlook.com
 * @Date: 2024-05-08 08:28:32
 * @LastEditors: ica caijianling@outlook.com
 * @LastEditTime: 2024-05-08 08:33:27
 * @FilePath: \dy-llm-voice\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
# dy-llm-voice

#### 介绍
用haodong108/dy-barrage-grab的ws将抖音弹幕通过大语言模型接口对话后返回的文字通过TTS接口输出

#### 软件架构
软件架构说明
python


#### 安装教程

1.  创建python环境
2.  pip install -r requirements.txt
3.  xxxx

#### 使用说明

1.  复制一份config-template.json为config.json并填写接口、key等信息
2.  运行[haodong108/dy-barrage-grab](https://gitee.com/haodong108/dy-barrage-grab)
3.  运行python async_main.py

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
